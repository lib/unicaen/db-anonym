Module `unicaen/db-anonym`
==========================

Principe
--------

Ce module permet d'anonymiser les données de la bdd principale sur laquelle pointe une appli. Il permet aussi de
restaurer les données initiales (même si dans la plupart des cas d'usage l'anonymisation sera faite de façon définitive
lors de la préparation d'une bdd de démo par exemple).

Les données fictives utilisées pour anonymiser sont générées à l'aide de [FakerPHP](https://github.com/FakerPHP/Faker).

Les tables et colonnes à traiter sont spécifiées dans un fichier de config, en terme de classes d'entités et d'attributs
Doctrine. Cf. [exemple de config](./config/unicaen-db-anonym.local.php.dist).

Il est possibles d'écarter certains enregistrements de ces tables selon la valeur d'une colonne
(cf. clé de config `'except'`).

Seules les valeurs de colonnes non null sont anonymisées.


Préalables
----------

- Une table `unicaen_db_anonym` est utilisée pour enregistrer le fait que la bdd a été anonymisée
ou non. *Cela empêche qu'un script de restauration ne soit généré à partir d'une bdd anonymisée, auquel cas on ne pourrait
pas restaurer les données d'origine !*
Le script de création de cette table dans une bdd Postgres est fourni [ici](./sql/schema.postgres.sql).


Actions disponibles
-------------------

### Génération des scripts d'anonymisation/restauration

Le module génére 2 scripts SQL :
- 1 script d'*anonymisation* des données, dont le chemin est spécifié par la clé de config `['output']['anonymisation']`) ;
- 1 script de *restauration* des données originales, dont le chemin est spécifié par la clé de config `['output']['restauration']`).

L'idée est de parcourir tous les enregistrements de chaque entité/table (sauf ceux à écarter) et de 
générer pour chacun :
  - d'une part un `update ... set ... where id = ...` d'anonymisation (inscrit dans le script d'anonymisation) ;
  - d'autre part un `update ... set ... where id = ...` de restauration (inscrit dans le script de restauration).

Aperçu d'un script d'anonymisation :
```sql
update DOCTORANT set INE = '646f9de9fcca4a99bab2690608082e4b' where id = 48868 ;
update DOCTORANT set INE = '3a183d8d7279e1a96b9d28ef946f954a' where id = 48910 ;
update DOCTORANT set INE = '97e795820dc7ad554861f4506560bd6c' where id = 48892 ;
update DOCTORANT set INE = '4099c33d8adbde2cfbc8935f37daecf1' where id = 30070 ;
update DOCTORANT set INE = '969deb1e7289759e3003c45cea027a42' where id = 30071 ;
update DOCTORANT set INE = 'd6026025bcbff07930ffb98e349a4672' where id = 30073 ;
```

Aperçu du script de restauration associé :
```sql
update DOCTORANT set INE = '0BLTV200U26' where id = 48868 ;
update DOCTORANT set INE = '086OO5006D1' where id = 48910 ;
update DOCTORANT set INE = '03140E00MA4' where id = 48892 ;
update DOCTORANT set INE = '03140E00MZ9' where id = 30070 ;
update DOCTORANT set INE = '03140E00N22' where id = 30071 ;
update DOCTORANT set INE = '03140E00N33' where id = 30073 ;
```

Commande pour lancer la génération :

```bash
php public/index.php unicaen-db-anonym generer
```

### Lancement de l'anonymisation

Le module est en mesure d'exécuter le script d'anonymisation à condition que :
  - le témoin d'anonymisation est à '0' dans la table `unicaen_db_anonym`,
  - le script d'anonymisation **et** celui de restauration ont été générés.

```bash
php public/index.php unicaen-db-anonym anonymiser
```

À l'issue de l'anonymisation, le témoin d'anonymisation est mis à '1' dans la table `unicaen_db_anonym`, ce qui empêchera de
lancer inutilement une nouvelle anonymisation mais surtout de regénérer un script de restauration à partir de la bdd
anonymisée.

### Lancement de la restauration

Le module est en mesure d'exécuter le script de restauration à condition que :
  - le témoin d'anonymisation est à '1' dans la table `unicaen_db_anonym`,
  - le script d'anonymisation **et** celui de restauration ont été générés.

```bash
php public/index.php unicaen-db-anonym restaurer
```

À l'issue de la restauration, le témoin d'anonymisation est mis à '0' dans la table `unicaen_db_anonym`, ce qui empêchera de
lancer inutilement une nouvelle restauration.


Remarques importantes
---------------------

- Les scripts d'anonymisation/restauration ne visent que les enregistrements qui existaient dans la bdd cible au moment
de leur génération. Donc si les données de cette bdd évoluent dans le temps (par exemple à l'issue d'un import de données 
périodique ou au fil de l'utilisation de l'appli), ces scripts n'impacteront pas les nouveaux
enregistrements apparus ou planteront sur les enregistrements disparus.
