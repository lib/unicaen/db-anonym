<?php

namespace UnicaenDbAnonym;

use UnicaenPrivilege\Guard\PrivilegeController;
use UnicaenDbAnonym\Controller\ConsoleController;
use UnicaenDbAnonym\Controller\ConsoleControllerFactory;
use UnicaenDbAnonym\Controller\IndexController;
use UnicaenDbAnonym\Controller\IndexControllerFactory;
use UnicaenDbAnonym\Service\AnonymService;
use UnicaenDbAnonym\Service\AnonymServiceFactory;
use UnicaenDbAnonym\Service\DbService;
use UnicaenDbAnonym\Service\DbServiceFactory;
use UnicaenDbAnonym\Service\GeneratorService;
use UnicaenDbAnonym\Service\GeneratorServiceFactory;

return [
    'unicaen-db-anonym' => [
        'output' => [
            'anonymisation' => '/tmp/unicaen_db_anonym_anonymisation.sql',
            'restauration' => '/tmp/unicaen_db_anonym_restauration.sql',
        ],
        'entities' => [],
    ],
    'router' => [
        'routes' => [
            'unicaen-db-anonym' => [
                'type' => 'Literal',
                'options' => [
                    'route' => '/unicaen-db-anonym',
                    'defaults' => [
                        'controller' => IndexController::class,
                        'action' => 'index',
                    ],
                ],
                'may_terminate' => true,
                'child_routes' => [
                    'generer' => [
                        'type' => 'Literal',
                        'options' => [
                            'route' => '/generer',
                            'defaults' => [
                                'controller' => IndexController::class,
                                'action' => 'generer',
                            ],
                        ],
                    ],
                    'anonymiser' => [
                        'type' => 'Literal',
                        'options' => [
                            'route' => '/anonymiser',
                            'defaults' => [
                                'controller' => IndexController::class,
                                'action' => 'anonymiser',
                            ],
                        ],
                    ],
                    'restaurer' => [
                        'type' => 'Literal',
                        'options' => [
                            'route' => '/restaurer',
                            'defaults' => [
                                'controller' => IndexController::class,
                                'action' => 'restaurer',
                            ],
                        ],
                    ],
                ],
            ],
        ],
    ],
    'navigation' => [
        'default' => [
            'home' => [
                'pages' => [

                ],
            ],
        ],
    ],
    'console' => [
        'router' => [
            'routes' => [
                'generer' => [
                    'options' => [
                        'route'    => 'unicaen-db-anonym generer',
                        'defaults' => [
                            /**
                             * @see ConsoleController::genererAction()
                             */
                            'controller' => ConsoleController::class,
                            'action'     => 'generer',
                        ],
                    ],
                ],
                'anonymiser' => [
                    'options' => [
                        'route'    => 'unicaen-db-anonym anonymiser',
                        'defaults' => [
                            /**
                             * @see ConsoleController::anonymiserAction()
                             */
                            'controller' => ConsoleController::class,
                            'action'     => 'anonymiser',
                        ],
                    ],
                ],
                'restaurer' => [
                    'options' => [
                        'route'    => 'unicaen-db-anonym restaurer',
                        'defaults' => [
                            /**
                             * @see ConsoleController::restaurerAction()
                             */
                            'controller' => ConsoleController::class,
                            'action'     => 'restaurer',
                        ],
                    ],
                ],
            ],
        ],
    ],
    'bjyauthorize' => [
        'guards' => [
            PrivilegeController::class => [
                [
                    /**
                     * @see ConsoleController::genererAction()
                     */
                    'controller' => ConsoleController::class,
                    'action' => [
                        'generer',
                        'anonymiser',
                        'restaurer',
                    ],
                    'role' => [],
                ],
                [
                    /**
                     * @see IndexController::indexAction()
                     */
                    'controller' => IndexController::class,
                    'action' => [
                        'index',
                        'generer',
                        'anonymiser',
                        'restaurer',
                    ],
                    'role' => [],
                ],
                [
                    'controller' => 'DoctrineModule\Controller\Cli',
                    'role' => [],
                ],
            ],
        ],
    ],
    'service_manager' => [
        'factories' => [
            DbService::class => DbServiceFactory::class,
            AnonymService::class => AnonymServiceFactory::class,
            GeneratorService::class => GeneratorServiceFactory::class,
        ],
    ],
    'controllers' => [
        'factories' => [
            IndexController::class => IndexControllerFactory::class,
            ConsoleController::class => ConsoleControllerFactory::class,
        ],
    ],
    'view_manager' => [
        'template_path_stack' => [
            __DIR__ . '/../view',
        ],
    ],
];