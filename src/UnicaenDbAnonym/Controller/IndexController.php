<?php

namespace UnicaenDbAnonym\Controller;

use Application\Controller\AbstractController;
use Exception;
use Laminas\View\Model\ViewModel;
use RuntimeException;
use UnicaenSql\Service\SQL\RunSQLResult;
use UnicaenDbAnonym\Service\AnonymServiceAwareTrait;

class IndexController extends AbstractController
{
    use AnonymServiceAwareTrait;

    public function indexAction(): array
    {
        $anonymisationScriptPath = $this->anonymService->getAnonymisationScriptPath();
        $restaurationScriptPath = $this->anonymService->getRestaurationScriptPath();

        $data = [];
        if (file_exists($anonymisationScriptPath) && file_exists($restaurationScriptPath)) {
            $data['anonymisationScriptPath'] = $anonymisationScriptPath;
            $data['restaurationScriptPath'] = $restaurationScriptPath;
        } else {
            $data['anonymisationScriptPath'] = null;
            $data['restaurationScriptPath'] = null;
        }

        return $data;
    }

    public function genererAction(): array
    {
        $anonymisationScriptPath = $this->anonymService->getAnonymisationScriptPath();
        $restaurationScriptPath = $this->anonymService->getRestaurationScriptPath();
        $messages = [];

        try {
            $gen = $this->anonymService->genererScripts();
            foreach ($gen as ['table' => $table, 'fields' => $fields, 'count' => $count]) {
                $messages[] = sprintf("- Table %s (colonnes %s) : %d lignes", $table, implode(', ', $fields), $count);
            }
        } catch (Exception $e) {
            throw new RuntimeException("Une erreur est survenue pendant la génération des scripts SQL.", null, $e);
        }

        return [
            'messages' => $messages,
            'anonymisationScriptPath' => $anonymisationScriptPath,
            'restaurationScriptPath' => $restaurationScriptPath,
        ];
    }

    public function anonymiserAction(): ViewModel
    {
        $scriptPath = $this->anonymService->getAnonymisationScriptPath();

        try {
            $result = $this->anonymService->anonymiser();
        } catch (Exception $e) {
            throw new RuntimeException("Une erreur est survenue lors du lancement du script '$scriptPath'.", null, $e);
        }

        return $this->resultViewModel($result);
    }

    public function restaurerAction(): ViewModel
    {
        $scriptPath = $this->anonymService->getRestaurationScriptPath();

        try {
            $result = $this->anonymService->restaurer();
        } catch (Exception $e) {
            throw new RuntimeException("Une erreur est survenue lors du lancement du script '$scriptPath'.", null, $e);
        }

        return $this->resultViewModel($result);
    }

    protected function resultViewModel(RunSQLResult $result): ViewModel
    {
        $vm = new ViewModel([
            'result' => $result,
        ]);
        $vm ->setTemplate('unicaen-db-anonym/index/result');

        return $vm;
    }
}