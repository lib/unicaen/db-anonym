<?php

namespace UnicaenDbAnonym\Controller;

use Exception;
use RuntimeException;
use Unicaen\Console\Controller\AbstractConsoleController;
use UnicaenDbAnonym\Service\AnonymServiceAwareTrait;

class ConsoleController extends AbstractConsoleController
{
    use AnonymServiceAwareTrait;

    public function genererAction(): void
    {
        $start = microtime(true);

        $this->console->writeLine("Generation des scripts SQL...");

        $anonymisationScriptPath = $this->anonymService->getAnonymisationScriptPath();
        $restaurationScriptPath = $this->anonymService->getRestaurationScriptPath();
        try {
            $gen = $this->anonymService->genererScripts();
            foreach ($gen as ['entity' => $entity, 'fields' => $fields, 'count' => $count]) {
                $this->console->writeLine(
                    sprintf("- Entite %s (%s) : %d lignes traitees", $entity, implode(', ', $fields), $count)
                );
            }
        } catch (Exception $e) {
            throw new RuntimeException("Une erreur est survenue pendant la génération des scripts SQL.", null, $e);
        }

        $end = microtime(true);

        $this->console->writeLine(sprintf("> Script d'anonymisation cree : %s", realpath($anonymisationScriptPath)));
        $this->console->writeLine(sprintf("> Script de restauration cree : %s", realpath($restaurationScriptPath)));
        $this->console->writeLine(sprintf("Duree : %.2f s", $end - $start));
    }

    public function anonymiserAction(): void
    {
        $scriptPath = $this->anonymService->getAnonymisationScriptPath();

        $this->console->writeLine("Lancement du script d'anonymisation '$scriptPath'...");

        $start = microtime(true);
        try {
            $result = $this->anonymService->anonymiser();
        } catch (Exception $e) {
            throw new RuntimeException("Une erreur est survenue lors du lancement du script '$scriptPath'.", null, $e);
        }
        $end = microtime(true);

        $this->console->writeLine("> Résultat : " . ($result->isSuccess() ? 'Succès' : 'Échec'));
        $this->console->writeLine("> Log file : " . $result->getLogFilePath());
        $this->console->writeLine(sprintf("Duree : %.2f s", $end - $start));
    }

    public function restaurerAction(): void
    {
        $scriptPath = $this->anonymService->getRestaurationScriptPath();

        $this->console->writeLine("Lancement du script de restauration '$scriptPath'...");

        $start = microtime(true);
        try {
            $result = $this->anonymService->restaurer();
        } catch (Exception $e) {
            throw new RuntimeException("Une erreur est survenue lors du lancement du script '$scriptPath'.", null, $e);
        }
        $end = microtime(true);

        $this->console->writeLine("> Résultat : " . ($result->isSuccess() ? 'Succès' : 'Échec'));
        $this->console->writeLine("> Log file : " . $result->getLogFilePath());
        $this->console->writeLine(sprintf("Duree : %.2f s", $end - $start));
    }
}