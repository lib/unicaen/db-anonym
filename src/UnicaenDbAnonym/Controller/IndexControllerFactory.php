<?php

namespace UnicaenDbAnonym\Controller;

use UnicaenDbAnonym\Service\AnonymService;
use Psr\Container\ContainerInterface;

class IndexControllerFactory
{
    /**
     * Create service
     *
     * @param ContainerInterface $container
     * @return IndexController
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function __invoke(ContainerInterface $container): IndexController
    {
        /** @var \UnicaenDbAnonym\Service\AnonymService $service */
        $service = $container->get(AnonymService::class);

        $controller = new IndexController();
        $controller->setAnonymService($service);

        return $controller;
    }
}