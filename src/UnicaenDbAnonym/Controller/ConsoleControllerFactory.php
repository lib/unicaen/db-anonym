<?php

namespace UnicaenDbAnonym\Controller;

use UnicaenDbAnonym\Service\AnonymService;
use Psr\Container\ContainerInterface;

class ConsoleControllerFactory
{
    /**
     * @param \Psr\Container\ContainerInterface $container
     * @return \UnicaenDbAnonym\Controller\ConsoleController
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function __invoke(ContainerInterface $container): ConsoleController
    {
        /** @var \UnicaenDbAnonym\Service\AnonymService $service */
        $dbFakeService = $container->get(AnonymService::class);

        $controller = new ConsoleController();
        $controller->setAnonymService($dbFakeService);

        return $controller;
    }
}