<?php

namespace UnicaenDbAnonym\Provider\fr_FR;

use Faker\Provider\fr_FR\Person;
use Individu\Entity\Db\Individu;

class PersonExtension extends Person
{
    /**
     * Remplacement de 'firstName()' pour prendre en compte la 'civilite' éventuelle dans le contexte.
     */
    public function firstName($gender = null, ?array $context = null): string
    {
        if ($gender !== null) {
            return parent::firstName($gender);
        }

        $civilite = $context['civilite'] ?? null;
        if ($civilite === null) {
            return parent::firstName($gender);
        }

        $gender = [Individu::CIVILITE_MME => 'female', Individu::CIVILITE_M => 'male'][$civilite] ?? null;

        return parent::firstName($gender);
    }
}