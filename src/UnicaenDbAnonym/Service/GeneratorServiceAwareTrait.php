<?php

namespace UnicaenDbAnonym\Service;

trait GeneratorServiceAwareTrait
{
    protected GeneratorService $generatorService;

    public function setGeneratorService(GeneratorService $generatorService): void
    {
        $this->generatorService = $generatorService;
    }
}