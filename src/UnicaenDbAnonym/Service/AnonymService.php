<?php

namespace UnicaenDbAnonym\Service;

use Generator;
use UnicaenSql\Service\SQL\RunSQLResult;
use Webmozart\Assert\Assert;
use Webmozart\Assert\InvalidArgumentException;

/**
 * @property \Psr\Log\LoggerInterface $logger
 */
class AnonymService
{
    use DbServiceAwareTrait;

    protected array $entitiesConfig = [];
    protected array $outputConfig = [];
    protected array $processedEntities = [];

    public function setConfig(array $config): self
    {
        $this->entitiesConfig = $config['entities'];
        $this->outputConfig = $config['output'];

        return $this;
    }

    public function getAnonymisationScriptPath()
    {
        return $this->outputConfig['anonymisation'];
    }

    public function getRestaurationScriptPath()
    {
        return $this->outputConfig['restauration'];
    }

    public function genererScripts(): Generator
    {
        if ($this->dbService->isBddAnonymisee()) {
            throw new InvalidArgumentException(
                "Génération interdite car la base de données est déjà anonymisée (cf. témoin dans la table des metadata)");
        }

        $anonymisationFilePath = $this->getAnonymisationScriptPath();
        $restaurationFilePath = $this->getRestaurationScriptPath();

        if (file_exists($anonymisationFilePath)) {
            throw new InvalidArgumentException("Le fichier '$anonymisationFilePath' existe déjà !");
        }
        if (file_exists($restaurationFilePath)) {
            throw new InvalidArgumentException("Le fichier '$restaurationFilePath' existe déjà !");
        }

        Assert::notEmpty($this->entitiesConfig, "Aucune classe d'entité spécifiée dans la config");

        $mf = fopen($anonymisationFilePath, 'w');
        $rf = fopen($restaurationFilePath, 'w');

        foreach ($this->entitiesConfig as $entityName => $entityConfig) {
            Assert::keyExists($entityConfig, $k = 'mapping', "La config de l'entité '$entityName' doit posséder la clé '$k'");

            $mapping = $this->dbService->normalizeMapping($entityName, $entityConfig['mapping']);
            $fields = array_keys($mapping);
            $except = $entityConfig['except'] ?? [];

            $records = $this->dbService->fetchEntityRecords($entityName, $fields, $except);

            [$migrateSqlLines, $restoreSqlLines] = $this->dbService->generateSqlLines($entityName, $records, $mapping);

            fputs($mf, implode(PHP_EOL, $migrateSqlLines) . PHP_EOL);
            fputs($rf, implode(PHP_EOL, $restoreSqlLines) . PHP_EOL);

            yield [
                'entity' => $entityName,
                'fields' => $fields,
                'count' => count($migrateSqlLines),
            ];

            unset ($records);
        }

        fclose($mf);
        fclose($rf);
    }

    /**
     * @throws \Doctrine\DBAL\ConnectionException
     * @throws \Doctrine\DBAL\Exception
     */
    public function anonymiser(): RunSQLResult
    {
        if ($this->dbService->isBddAnonymisee()) {
            throw new InvalidArgumentException(
                "Anonymisation interdite car la base de données est déjà anonymisée (cf. témoin dans la table des metadata)");
        }

        $scriptPath = $this->getAnonymisationScriptPath();
        $result = $this->dbService->lancerScript($scriptPath);
        $this->dbService->setBddAnonymisee(true);

        return $result;
    }

    /**
     * @throws \Doctrine\DBAL\ConnectionException
     * @throws \Doctrine\DBAL\Exception
     */
    public function restaurer(): RunSQLResult
    {
        if (! $this->dbService->isBddAnonymisee()) {
            throw new InvalidArgumentException(
                "Restauration interdite car la base de données n'est pas anonymisée (cf. témoin dans la table des metadata)");
        }

        $scriptPath = $this->getRestaurationScriptPath();
        $result = $this->dbService->lancerScript($scriptPath);
        $this->dbService->setBddAnonymisee(false);

        return $result;
    }
}
