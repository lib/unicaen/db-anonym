<?php

namespace UnicaenDbAnonym\Service;

use Psr\Container\ContainerInterface;

class DbServiceFactory
{
    /**
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     * @throws \Doctrine\DBAL\Exception
     */
    public function __invoke(ContainerInterface $container): DbService
    {
        $service = new DbService;

        /** @var \Doctrine\ORM\EntityManager $em */
        $em = $container->get('doctrine.entitymanager.orm_default');
        $service->setEntityManager($em);

        /** @var \UnicaenDbAnonym\Service\GeneratorService $generatorService */
        $generatorService = $container->get(GeneratorService::class);
        $service->setGeneratorService($generatorService);

        return $service;
    }
}