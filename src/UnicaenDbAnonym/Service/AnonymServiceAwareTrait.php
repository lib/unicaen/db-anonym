<?php

namespace UnicaenDbAnonym\Service;

trait AnonymServiceAwareTrait
{
    /**
     * @var AnonymService
     */
    protected AnonymService $anonymService;

    /**
     * @param AnonymService $anonymService
     * @return self
     */
    public function setAnonymService(AnonymService $anonymService): self
    {
        $this->anonymService = $anonymService;
        return $this;
    }


}