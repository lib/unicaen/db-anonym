<?php

namespace UnicaenDbAnonym\Service;

use Psr\Container\ContainerInterface;
use Webmozart\Assert\Assert;

class AnonymServiceFactory
{
    /**
     * @param \Psr\Container\ContainerInterface $container
     * @return \UnicaenDbAnonym\Service\AnonymService
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function __invoke(ContainerInterface $container): AnonymService
    {
        /** @var DbService $dbService */
        $dbService = $container->get(DbService::class);

        $config = $this->getConfig($container);

        $service = new AnonymService();
        $service->setConfig($config);
        $service->setDbService($dbService);

        return $service;
    }

    /**
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    private function getConfig(ContainerInterface $container): array
    {
        /** @var array $appConfig */
        $appConfig = $container->get('Config');

        $config = $appConfig['unicaen-db-anonym'] ?? [];
        if (!$config) {
            return [];
        }

        Assert::isArray($config, "La config du module doit être un tableau");
        Assert::keyExists($config, $k = 'entities', "La config du module doit posséder la clé '$k'");
        Assert::keyExists($config, $k = 'output', "La config du module doit posséder la clé '$k'");

        return $config;
    }
}