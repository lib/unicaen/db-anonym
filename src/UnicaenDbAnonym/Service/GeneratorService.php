<?php

namespace UnicaenDbAnonym\Service;

use Faker\Generator;

class GeneratorService
{
    private Generator $faker;

    public function setFaker(Generator $faker): void
    {
        $this->faker = $faker;
    }

    /**
     * Pour une {@see $seed} non null donnée, permet d'obtenir toujours les mêmes valeurs générées.
     * @see http://fakerphp.org/#seeding-the-generator
     */
    public function seed($seed): void
    {
        $this->faker->seed($seed);
    }

    /**
     * Resets the unique modifier for all providers (unique() keeps one array of values per provider).
     * @see http://fakerphp.org/#modifiers
     */
    public function resetUniqueModifier(): void
    {
        $this->faker->unique(true);
    }

    /**
     * Génère une valeur fictive aléatoire selon la config spécifiée.
     *
     * @param array $config Config, exemples :
     * ```
     * 'lastName', // càd `$faker->lastName()`
     * 'null', // càd mise à NULL.
     * ['words', 3, true], // càd `$faker->words(3, true)`
     * ['name' => 'words', 'unique' => true, 'params' => [3, true]], // càd `$faker->unique()->words(3, true)`
     * ['name' => 'randomElement', 'params' => [Individu::CIVILITE_MME, Individu::CIVILITE_M]],
     * ['name' => 'firstName', 'params' => [null], 'context' => true] // càd `$faker->firstName(null, $context)` // Cf. {@see \UnicaenDbAnonym\Provider\fr_FR\PersonExtension}
     * ```
     * @param array $context Contexte : autres valeurs générées jusqu'à présent
     */
    public function generateFakeValue(array $config, array $context = []): string
    {
        // normalisation
        if (!array_key_exists('name', $config)) {
            $method = array_shift($config);
            $config = ['name' => $method, 'params' => array_values($config)];
        }
        if (!array_key_exists('unique', $config)) {
            $config['unique'] = false;
        }
        if (!array_key_exists('context', $config)) {
            $config['context'] = false;
        }

        $method = $config['name'];
        if ($method === 'null') {
            return 'NULL';
        }

        if ($config['context'] === true && !array_key_exists('params', $config)) {
            throw new \InvalidArgumentException("Si la clé 'context' est à true, la clé 'params' doit être présente (méthode '$method').");
        }

        $params = $config['params'] ?? [];
        $unique = (bool) $config['unique'];

        // si demandé, passage du contexte à la méthode (en dernier argument)
        if ($config['context'] === true) {
            $params[] = $context;
        }

        if ($unique) {
            $fakeValue = $this->faker->unique()->$method(...$params);
        } else {
            $fakeValue = $this->faker->$method(...$params);
        }

        return $fakeValue;
    }
}