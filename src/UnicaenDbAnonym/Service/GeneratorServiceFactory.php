<?php

namespace UnicaenDbAnonym\Service;

use Faker\Factory;
use Locale;
use Psr\Container\ContainerInterface;
use UnicaenDbAnonym\Provider\fr_FR\PersonExtension;

class GeneratorServiceFactory
{
    public function __invoke(ContainerInterface $container): GeneratorService
    {
        $service = new GeneratorService();

        $faker = Factory::create(Locale::getDefault());
        $faker->addProvider(new PersonExtension($faker));
        $service->setFaker($faker);

        return $service;
    }
}