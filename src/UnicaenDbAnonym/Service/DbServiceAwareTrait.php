<?php

namespace UnicaenDbAnonym\Service;

trait DbServiceAwareTrait
{
    /**
     * @var DbService $dbService
     */
    protected DbService $dbService;

    public function setDbService(DbService $dbService)
    {
        $this->dbService = $dbService;
    }
}