CHANGELOG
=========

4.0.0
-----
- Requiert la nouvelle biblilothèque unicaen/privilege.

3.1.0
-----
- Config : possibilité d'utiliser des noms de colonnes en plus des noms d'attributs Doctrine.
- Config : support d'un format plus explicite permettant de spécifier le modificateur Faker 'unique'.
- Possibilité de transmettre le contexte (valeurs générées jusqu'à présent) à une méthode maison (ex: PersonExtension::firstName).
- Suppression des dépendances à unicaen/app en faveur de la nouvelle bibliothèque unicaen/sql.
- Renommage de la table _METADATA en `unicaen_db_anonym`.
- [FIX] Plus d'anonymisation si la valeur est null.

3.0.0
-----
- PHP 8 requis

2.0.0
-----
- [FIX] CHemins liés à unicaen/console remplaçant laminas/console
- [FIX] Problèmes signalés par PHPStorm
 
1.0.0
-----
- Première version.