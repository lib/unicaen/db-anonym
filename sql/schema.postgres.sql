--
-- Création si nécessaire de la table des métadonnées de la bdd.
--
create table if not exists unicaen_db_anonym (
    key varchar(64) not null,
    value text,
    extra text,
    description varchar(128)
);
-- Création du témoin d'anonymisation
insert into unicaen_db_anonym (key, value, description)
values ('BDD_ANONYMISEE', '0', 'Indique si les données de cette bdd ont été anonymisées (1) ou non (0)');
