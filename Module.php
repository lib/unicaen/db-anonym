<?php

namespace UnicaenDbAnonym;

use Laminas\Config\Factory as ConfigFactory;
use Laminas\Stdlib\Glob;
use Unicaen\Console\Adapter\AdapterInterface;

class Module
{
    public function getConfig(): array
    {
        $paths = array_merge(
            [__DIR__ . '/config/module.config.php'],
            Glob::glob(__DIR__ . '/config/others/{,*.}{config}.php', Glob::GLOB_BRACE)
        );

        return ConfigFactory::fromFiles($paths);
    }

    public function getAutoloaderConfig(): array
    {
        return array(
            'Laminas\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
                ),
            ),
        );
    }

    /**
     * @inheritDoc
     */
    public function getConsoleBanner(AdapterInterface $console): ?string
    {
        return "UnicaenDbAnonym";
    }

    /**
     * @inheritDoc
     */
    public function getConsoleUsage(AdapterInterface $console): array
    {
        return [
            /**
             * @see ConsoleController::genererAction()
             */
            'unicaen-db-anonym generer' => "Génère le script d'anonymisation et le script de restauration.",

            /**
             * @see ConsoleController::anonymiserAction()
             */
            'unicaen-db-anonym anonymiser' => "Lance le script SQL d'anonymisation généré précédemment.",
            [],

            /**
             * @see ConsoleController::restaurerAction()
             */
            'unicaen-db-anonym restaurer' => "Lance le script SQL de restauration généré précédemment.",
            [],
        ];
    }
}
